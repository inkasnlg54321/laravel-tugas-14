@extends('layout.master')  
@section('title')
    Halaman Show Data {ID} {{$cast-> id}}
@endsection

@section('isi')
    <h4>Nama Cast   : {{$cast->nama}}</h4>
    <h4>Umur        : {{$cast->umur}}</h4>
    <h4>Bio         : {{$cast->bio}}</h4>
@endsection