@extends('layout.master')  
@section('title')
    Halaman Edit Data Cast {ID} {{$cast-> id}}
@endsection

@section('isi')
<h2>Tambah Data</h2>
        <form action="/cast/{{$cast -> id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="nama">Nama Cast</label>
                <input type="text" class="form-control" name="nama" value ="{{$cast -> nama}}" id="nama" placeholder="Masukkan nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="text" class="form-control" name="umur" value ="{{$cast -> umur}}" id="umur" placeholder="Masukkan umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <input type="text" class="form-control" name="bio" value ="{{$cast -> bio}}" id="bio" placeholder="Masukkan bio">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
@endsection

