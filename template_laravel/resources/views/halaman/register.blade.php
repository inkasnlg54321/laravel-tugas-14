@extends('layout.master')  
@section('title')
    Halaman Form
@endsection

@section('isi')
    <form action="/kirim" method="post">
      @csrf
      <h3>First Name : </h3>
      <input type="text" name="firstname">

      <h3>Last Name : </h3>
      <input type="text" name="lastname">

      <h3>Gender : </h3>
      <input type="radio">
      <label>Bahasa Indonesia</label><br>
      <input type="radio">
      <label> English</label><br>
      <input type="radio">
      <label> Other</label>

      <h3>Nationality : </h3>
      <select>
          <option>Indonesian</option>
          <option>English</option>
          <option>Europe</option>
          <option>US</option>
          <option>Afrika</option>
      </select>

      <h3>Lenguage Spoken : </h3>
      <input type="checkbox">
      <label>Bahasa Indonesia</label><br>
      <input type="checkbox">
      <label> English</label><br>
      <input type="checkbox">
      <label> Other</label>

      <h3>Bio : </h3>
      <textarea id="Bio" name="Bio" cols="40" rows="8" aria-required="true"></textarea>
      <br>
      <a href="/welcome"><button>Sign Up</button></a>
    </form>
@endsection
